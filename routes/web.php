<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group([
	'namespace' => 'Front'
	], function() {
	Route::get('/', 'IndexController');
});

// backend login
Route::group([
		'namespace' => 'Auth',
		'prefix' => 'auth',
	], function(){
	Route::get('/', 'AdminLoginController@index');
	Route::post('/', 'AdminLoginController@login')->name('admLogin');
});

Auth::routes();

Route::group([
	'namespace' => 'Front',
	'middleware' => 'auth',
	], function() {
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/game', 'HomeController@startGame')->name('game');
	Route::post('/question', 'GameController@getQuestion')->name('question');

});


Route::group([
	'namespace' => 'Back',
	'middleware' => 'auth:admin',
	'prefix' => 'admin'
	], function() {
	Route::get('/dashboard', 'IndexController@index')->name('adminDashboard');
	Route::get('/questions', 'QuestionsController@index')->name('Questions');
	Route::get('/add-questions', 'QuestionsController@add')->name('AddQuestions');
	Route::post('/add-questions', 'QuestionsController@addQuestion')->name('AddNewQuestions');
	Route::get('/answers', 'AnswersController@index')->name('Answers');
});
