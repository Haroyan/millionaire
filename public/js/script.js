$(document).ready(function init(){
	$('button.answer').on('click', function(){
		let answer = $(this);
		let question = $('#question').text();
		$.ajax({
			headers: {
	          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        },
			url: "/question",
			data: {
				answer: answer.text(),
				question: question,
			},
			method: "post",
			dataType: 'json',
			success: function(res){
				rightAnswer(res);
				init();
			}, error: function(res) {
				$(answer).removeClass("form-control");
				$(answer).addClass("alert-danger");
				$('#message').addClass("alert-danger");
				$('#message p').html("false answer");
				setTimeout(function(){
					alert("you lose the right anser was "+res.responseText);
					location.reload();
				}, 500);
			}
		});
	});

	function rightAnswer(res) {
		if (res === true) {
			alert("you win");
			location.reload();
		}
		$('#answers').html('');
		$('#question').html(res.question);
		$('#score').html(res.user_score);
		for(let i = 0; i < 4; i++) {
			$('#answers').append("<div class='col-md-6'><div class='form-group'><button class='form-control answer'>"+res[i]+"</button></div></div>");
		}
	}
});