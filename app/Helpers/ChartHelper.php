<?php

namespace App\Helpers;
use App\Models\Chart;

class ChartHelper
{
    public static function getCharts()
    {
    	$chart = Chart::orderBy('max_score', 'desc')->get();
    	return $chart;
    }
}
