<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
    protected $table = "questions";

    protected $fillable = [
    	'question',
    	'score',
    	'answer_id',
    ];

    public static function rules()
	{
		return [
			'question'   => 'required|max:50|min:2',
			'score'  => 'required|integer|max:20|min:5',
		];
	}
}
