<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answers extends Model
{
    protected $table = "answers";

    protected $fillable = [
    	'right_answer',
    	'wrong_answer1',
    	'wrong_answer2',
    	'wrong_answer3',
    ];

    public static function rules()
	{
		return [
			'right_answer'    => 'required|max:50|min:2',
			'wrong_answer1'   => 'required|max:50|min:2',
			'wrong_answer2'   => 'required|max:50|min:2',
			'wrong_answer3'   => 'required|max:50|min:2',
		];
	}
}
