<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Questions;
use App\Models\Answers;
use App\Helpers\Chart;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('front.home');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function startGame(Request $request)
    {
        $request->session()->put('questions', []);
        $request->session()->put('score', 0);
        $question = Questions::inRandomOrder()->take(1)->first();
        $id = $question->answer_id;
        $answer = Answers::findOrFail($id)->toArray();
        unset($answer['id']);
        unset($answer['created_at']);
        unset($answer['updated_at']);
        shuffle($answer);
        return view('front.game', [
            'answer' => $answer,
            'question' => $question
        ]);
    }

    public function getChart()
    {
    	
    }
}
