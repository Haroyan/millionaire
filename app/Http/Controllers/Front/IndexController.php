<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
	/**
     * Show start page
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
    	return view('front.index');
    }
}
