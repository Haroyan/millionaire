<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\Questions;
use App\Models\Answers;
use App\Models\Chart;

class GameController extends Controller
{
    
    /**
     * getting Random questions
     *
     * @return void
     */
    public function getQuestion(Request $request)
    {
        $question = Questions::where('question', $request->question)->first();
        $answer = Answers::where('id', $question->answer_id)->first();
        
        if ($answer->right_answer == $request->answer) {
            $sessionQuestions = $request->session()->get('questions');
            $score = $request->session()->get('score');
            $sessionQuestions[] = $request->question;
            $request->session()->put('questions', $sessionQuestions);
            $score += $question->score;
            $request->session()->put('score', $score);
            $question = Questions::whereNotIn('question', $sessionQuestions)->inRandomOrder()->take(1)->first();
            $answer = Answers::findOrFail($question->answer_id)->toArray();
            unset($question->answer_id);
            
            $data = [];
            array_push($data, $answer['right_answer'], $answer['wrong_answer1'], $answer['wrong_answer2'], $answer['wrong_answer3']);
            shuffle($data);
            
            $data['question'] = $question->question;
            $data['question_score'] = $question['score'];
            $data['user_score'] = $score;
            if (count($sessionQuestions) == 5) {
                $this->score($score);
                return response(true)
                   ->header('Status', 200);
            }
            return response(json_encode($data))
                   ->header('Content-Type', 'text/json');
        } else {
            return response(json_encode($answer->right_answer))
                   ->header('Status', 400);
        }
    }

    /**
     * function score
     * 
     * @param string $score
     * @return void
     */
    private function score($score)
    {
        $id = Auth::user()->id;
        $row = Chart::where(['user_id' => $id])->get();
        if ($row != null && count($row) == 0) {
            $chart = new Chart();
            $chart->user_id = $id;
            $chart->max_score = $score;
            $chart->save();
        } elseif ($score > $row[0]->max_score) {
            $row[0]->user_id = $id;
            $row[0]->max_score = $score;
            $row[0]->save();
        }
    }
}
