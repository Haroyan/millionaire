<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Questions;
use App\Models\Answers;
use Validator;

class QuestionsController extends Controller
{
    /**
     * function index()
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Questions::all();
    	return view('back.questions', ['questions' => $questions]);
    }

    /**
     * function add()
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
    	return view('back.addQuestions');
    }

    /**
     * function add()
     *
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function addQuestion(Request $request)
    {
        $validator = Validator::make(['question' => $request->question, 'score' => $request->score], Questions::rules());
        if ($validator->fails() === true) {
            return redirect()->back()->with("danger", "soething went wrong");
        }
        $answer = new Answers();
        $answer->right_answer = $request->right_answer;
        $answer->wrong_answer1 = $request->wrong_answer1;
        $answer->wrong_answer2 = $request->wrong_answer2;
        $answer->wrong_answer3 = $request->wrong_answer3;
        $answer->save();

    	$question = new Questions();
    	$question->question  = $request->question;
    	$question->answer_id = $answer->id;
    	$question->score     = $request->score;
    	$question->save();

    	return redirect()->back()->with("success", "record created successfully");
    }
}
