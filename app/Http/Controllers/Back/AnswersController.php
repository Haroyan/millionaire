<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Answers;

class AnswersController extends Controller
{
	/**
     * Show the answers page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$answers = Answers::all();
    	return view('back.answers', ['answers' => $answers]);
    }
}
