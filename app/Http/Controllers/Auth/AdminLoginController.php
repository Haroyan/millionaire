<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class AdminLoginController extends Controller
{

	/**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest:admin');
    }

    /**
	 * Admin authorization view
	 *
	 * @return resource
     */
    public function index()
    {
    	return view('auth.adminLogin');
    }

    /**
	 * Admin authorization
	 * 
	 * @return redirect
     */
    public function login(Request $request)
    {
	    $this->validate($request, [
	        'email' => 'required|email',
	        'password' => 'required|min:6',
	    ]);
	    if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
	        return redirect()->intended(route('adminDashboard'));
	    }
	    return redirect()->back()->withInput($request->only('email', 'remember'));
    }
}
