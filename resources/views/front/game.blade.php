@extends('front.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h3 class="text-right" id="score"></h3>
            <div class="alert" id="message"><p class="text-center"></p></div>
            <div class="alert alert-success"><p class="text-center" id="question">{{ $question->question }}</p></div>
            <div class="row" id="answers">
                @foreach($answer as $value)
                    <div class="col-md-6">
                        <div class="form-group">
                            <button class="form-control answer">{{ $value }}</button>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
