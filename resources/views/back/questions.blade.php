@extends('back.layouts.app')

@section('content')
	<div class="col-sm-3">
		<table class="table table-striped">
			<tbody>
				@foreach($questions as $question)
					<tr><td>{{ $question->question }}</td></tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection