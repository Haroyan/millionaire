@extends('back.layouts.app')

@section('content')
	<div class="col-sm-3">
		<table class="table table-striped">
			<tbody>
				@foreach($answers as $answer)
					<tr><td>{{ $answer->answer }}</td></tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection