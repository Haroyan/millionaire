<div class="col-sm-3">
    <ul class="list-group ">
        <li class="list-group-item"><a href="{{ url('/admin/questions') }}">Questions</a></li>
        <li class="list-group-item"><a href="{{ url('/admin/add-questions') }}">Add Questions</a></li>
        <li class="list-group-item"><a href="{{ url('/admin/answers') }}">Answers</a></li>
    </ul>
</div>