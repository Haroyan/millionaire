@extends('back.layouts.app')

@section('content')
	<div class="col-sm-4">
		@include('back._includes.messages')
		<form action="{{ route('AddNewQuestions') }}" method="POST">
			@csrf
			<div class="form-group">
				<label for="question">question</label>
				<textarea class="form-control" type="text" name="question" id="question"></textarea>
			</div>
			<div class="form-group">
				<label for="answer">answer</label>
				<input class="form-control" type="text" name="right_answer" id="answer" />
			</div>
			<div class="form-group">
				<label for="wrong_answer1">wrong_answer1</label>
				<input class="form-control" type="text" name="wrong_answer1" id="wrong_answer1" />
			</div>
			<div class="form-group">
				<label for="wrong_answer2">wrong_answer2</label>
				<input class="form-control" type="text" name="wrong_answer2" id="wrong_answer2" />
			</div>
			<div class="form-group">
				<label for="wrong_answer3">wrong_answer3</label>
				<input class="form-control" type="text" name="wrong_answer3" id="wrong_answer3" />
			</div>
			<div class="form-group">
				<label for="score">score</label>
				<input class="form-control" type="text" name="score" id="score" />
			</div>
			<div class="form-group">
				<input type="submit" class="btn">
			</div>
		</form>
	</div>
@endsection